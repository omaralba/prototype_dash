﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    FindMousePosition MouseComp;
    Vector3 movementDirection;
    public float speed = 1;
    bool moving;


    // Start is called before the first frame update
    void Start()
    {
        MouseComp = GetComponent<FindMousePosition>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && moving == false)
        {
            moving = true;
            movementDirection = MouseComp.VectorToMouse();
        }
    }

    private void FixedUpdate()
    {
        if (moving == true)
        {
            transform.position += movementDirection.normalized * speed * Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            moving = false;
        }
    }
}
